﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class UIController : MonoBehaviour
{
    public static UIController Instance;

    [SerializeField] private GameObject _startButtonObject;
    [Space]
    
    [SerializeField] private Animator _perfectTextAnimator;
    [Space]

    private string[] _perfectTextTitle = { "PERFECT!", "AWESOME!", "AMAZING!", "WELL DONE!", "COOL!" };

    [SerializeField] private TextMeshProUGUI _perfectText;
    [SerializeField] private TextMeshProUGUI _currentLevelText;
    [Space]

    [SerializeField] private RectTransform _retryButton;
    [SerializeField] private RectTransform _nextButton;
    [Space]

    [SerializeField] private Image _panelImage;
    [SerializeField] private Image _progressBarImage;

    private GameManager _gameManager;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _gameManager = GameManager.Instance;

        _currentLevelText.text = _gameManager.GetCurrentLevel().ToString();
    }

    private void Update()
    {
        if(_gameManager.IsGameStarted())
        {
            UpdateProgressBar();
        }
    }

    private void UpdateProgressBar()
    {
        _progressBarImage.fillAmount = _gameManager.PlayerProgress();
    }

    public void StartButton()
    {
        _startButtonObject.SetActive(false);
        _gameManager.GameStart();
    }

    public void ResetButton()
    {
        _gameManager.LOAD_GENERAL();
    }

    public IEnumerator PerfectShoot()
    {
        int r = Random.Range(0, _perfectTextTitle.Length);
        _perfectText.text = _perfectTextTitle[r];
        _perfectTextAnimator.SetBool("play", true);

        _panelImage.color = new Color(1, 1, 1, 0);
        _panelImage.DOFade(0.65f, 0.1f);

        yield return new WaitForSeconds(0.1f);

        _perfectTextAnimator.SetBool("play", false);
        _panelImage.DOFade(0f, 0.1f);
    }


    public IEnumerator GameOver()
    {
        _progressBarImage.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);

        _retryButton.DOAnchorPosY(500f, 0.5f);

        _panelImage.color = new Color(1, 0, 0, 0);
        _panelImage.raycastTarget = true;
        _panelImage.DOFade(0.4f, 0.5f);
    }

    public IEnumerator GameWin()
    {
        _progressBarImage.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);

        _nextButton.DOAnchorPosY(500f, 0.5f);

        _panelImage.color = new Color(0, 0, 1, 0);
        _panelImage.raycastTarget = true;
        _panelImage.DOFade(0.4f, 0.5f);
    }
}
