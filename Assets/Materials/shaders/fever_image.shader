﻿Shader "Unlit/fever_image"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        
		_Color ("Color", Color) = (1,1,1,1)
        _MaxRadius ("MaxRadius", Float) = 0
        _MinRadius ("MinRadius", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha
        
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _Color;
            float _MaxRadius;
            float _MinRadius;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            float Unity_InverseLerp_float4(float A, float B, float T)
            {
                return (T - A)/(B - A);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;
                
                float currentFragmentRadius = distance(i.uv, float2(0.5, 0.5));
                float alpha = Unity_InverseLerp_float4(_MinRadius, _MaxRadius, currentFragmentRadius);
                col.a = alpha;

                return col;
            }
            ENDCG
        }
    }
}
