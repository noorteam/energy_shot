﻿using UnityEngine;
using Obi;

public class Leg : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [Space]

    [SerializeField] private ObiRopeExtrudedRenderer _rope;
    [Space]

    [SerializeField] private MeshRenderer _myMesh;
    [Space]

    [SerializeField] private BoxCollider _collider;
    [Space]

    private GameObject _lastConnectedBody;

    private Joint _myJoint;

    private PlayerMovement _playerMovement;
    
    private void Start()
    {
        _playerMovement = PlayerMovement.Instance;
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("clingWall"))
        {
            Vector3 collidedPosition = other.GetContact(0).point;
            MakeWallFixedJoint(collidedPosition);
        }
    }

    public void AddImpulse(float impulse)
    {
        FreezConstraints();

        RemoveJoint();
        LegState(true);

        new WaitForEndOfFrame();

        _rigidbody.AddForce(-transform.up * impulse, ForceMode.Acceleration);
    }


    private void MakeWallFixedJoint(Vector3 collidedPosition)
    {
        RemoveJoint();

        GameObject connectedBody = new GameObject("connectedBody");
        connectedBody.transform.position = collidedPosition;

        transform.position = connectedBody.transform.position;

        Rigidbody rigidbody = connectedBody.AddComponent<Rigidbody>();
        rigidbody.isKinematic = true;

        _lastConnectedBody = connectedBody;

        FixedJoint joint = gameObject.AddComponent<FixedJoint>();
        joint.connectedBody = rigidbody;

        PlayerLegController.Instance.MakeConfigurableJoint(_rigidbody);

        _myJoint = joint;
    }

    public void MakeFixedJoint()
    {
        UnfreezConstraints();

        LegState(false);
        RemoveJoint();

        transform.position = _playerMovement.transform.position;

        FixedJoint joint = gameObject.AddComponent<FixedJoint>();
        joint.connectedBody = _playerMovement._rigidbody;

        _myJoint = joint;
    }


    /*public void MakeConfigurableJoint()
    {
        UnfreezConstraints();

        LegState(false);
        RemoveJoint();

        ConfigurableJoint joint = gameObject.AddComponent<ConfigurableJoint>();

        joint.connectedBody = _playerMovement._rigidbody;
        joint.anchor = Vector3.zero;
        joint.axis = Vector3.zero;

        joint.autoConfigureConnectedAnchor = false;
        joint.connectedAnchor = Vector3.zero;

        JointDrive spring = new JointDrive();
        spring.positionSpring = 5000;
        spring.maximumForce = 3.402823e+38f;

        joint.yDrive = spring;
        joint.zDrive = spring;

        _myJoint = joint;
    }*/


    private void LegState(bool type)
    {
        _rope.enabled = type;
        _myMesh.enabled = type;
        _collider.enabled = type;
    }

    public void RemoveJoint()
    {
        Destroy(_myJoint);
        Destroy(_lastConnectedBody);
    }

    private void FreezConstraints()
    {
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
    }

    private void UnfreezConstraints()
    {
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;
    }
}
