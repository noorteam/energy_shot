﻿using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    [SerializeField] private GameObject _legs;
    private GameObject _lastCollided;
    private GameObject _lastParent;

    private GameManager _gameManager;
    private PlayerMovement _playerMovement;
    private PlayerFever _playerFever;

    private void Start()
    {
        _gameManager = GameManager.Instance;
        _playerMovement = PlayerMovement.Instance;
        _playerFever = PlayerFever.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("finish"))
        {
            _lastCollided = other.gameObject;
            _lastParent = other.gameObject;

            _gameManager.GameWin();
        }

        if (_lastCollided == other.gameObject || _lastParent == other.transform.parent.gameObject) return;

        if (other.CompareTag("perfect_point"))
        {
            _lastCollided = other.gameObject;
            _lastParent = other.transform.parent.gameObject;

            _gameManager.SpawnPerfectParticle(80, false);
            _gameManager.SpawnGlassParticle(other);

            if(!_playerFever.IsFeverMode())
            {
                _playerFever.ChangePerfectShootedCount(true);
            }

            StartCoroutine(UIController.Instance.PerfectShoot());

            Destroy(other.transform.parent.gameObject);
        }

        if (other.CompareTag("point"))
        {
            _lastCollided = other.gameObject;
            _lastParent = other.transform.parent.gameObject;

            if(!_playerFever.IsFeverMode())
            {
                _playerMovement.ReduceVelocity();
                _playerFever.ChangePerfectShootedCount(false);
            }

            _gameManager.SpawnGlassParticle(other);
        }

        if(other.gameObject.layer == 11)
        {
            if(other.CompareTag("fever_wall"))
            {
                _gameManager.SpawnFeverWallPartcile(other);

                VibrationController.Instance.FalureImpact();
            }
            else
            {
                if (_playerFever.IsFeverShooted())
                {
                    _gameManager.SpawnObstaclePartcile(other);
                    VibrationController.Instance.FalureImpact();
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_lastCollided == collision.gameObject) return;

        if (collision.gameObject.CompareTag("obstacle"))
        {
            _lastCollided = collision.gameObject;
            _gameManager.SpawnDieParticle();

            VibrationController.Instance.HeavyImpact();

            Destroy(gameObject);
            Destroy(_legs);
        }
    }
}
