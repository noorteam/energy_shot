﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerLegController : MonoBehaviour
{
    public static PlayerLegController Instance;

    private float _legsShootImpulse = 8500f;

    [HideInInspector] public Vector3 _shootCenter;

    [SerializeField] private Leg[] _legs;

    private List<ConfigurableJoint> _myJoints = new List<ConfigurableJoint>();

    private bool _legsShooted;

    private PlayerMovement _playerMovement;
    private GameManager _gameManager;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _playerMovement = PlayerMovement.Instance;
        _gameManager = GameManager.Instance;

        ShootLegs();
    }

    private void Update()
    {
        if (_gameManager.IsGameStarted())
        {
            _shootCenter = (_legs[0].transform.position + _legs[1].transform.position) / 2;
        }
    }


    public void ShootLegs()
    {
        _legsShooted = true;
        for (int i = 0; i < 2; i++)
        {
            _legs[i].AddImpulse(_legsShootImpulse);
        }
    }

    public void GatherLegs()
    {
        _legsShooted = false;
        for (int i = 0; i < 2; i++)
        {
            RemoveJoint();
            _legs[i].MakeFixedJoint();
        }
    }


    public void OffSprings()
    {
        for (int i = 0; i < _myJoints.Count; i++)
        {
            JointDrive drive = new JointDrive();
            drive.positionSpring = 0;
            drive.maximumForce = 3.402823e+38f;

            _myJoints[i].xDrive = drive;
            _myJoints[i].yDrive = drive;
        }
    }

    public void MakeConfigurableJoint(Rigidbody rigidbody)
    {
        ConfigurableJoint bodyJoint = _playerMovement._rigidbody.gameObject.AddComponent<ConfigurableJoint>();

        bodyJoint.connectedBody = rigidbody;
        bodyJoint.anchor = Vector3.zero;
        bodyJoint.axis = Vector3.zero;

        bodyJoint.autoConfigureConnectedAnchor = false;
        bodyJoint.connectedAnchor = Vector3.zero;

        JointDrive drive = new JointDrive();
        drive.positionSpring = 650;
        drive.maximumForce = 3.402823e+38f;

        bodyJoint.xDrive = drive;
        bodyJoint.yDrive = drive;

        _myJoints.Add(bodyJoint);
    }

    private void RemoveJoint()
    {
        for (int i = 0; i < _myJoints.Count; i++)
        {
            Destroy(_myJoints[i]);
            _myJoints.RemoveAt(i);
        }
    }

    public bool LegsShooted()
    {
        return _legsShooted;
    }
}
