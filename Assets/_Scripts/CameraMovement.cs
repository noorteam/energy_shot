﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    public static CameraMovement Instance;

    [SerializeField] private float _yOffset;
    [SerializeField] private float _moveSpeed = 15f;
    [SerializeField] private float _minFieldOfView;
    [SerializeField] private float _maxFieldOfView;
    private float _fieldOfView;

    WaitForSeconds _waitTime = new WaitForSeconds(0.001f);
    [Space]

    [SerializeField] private Transform[] _targets;

    private Camera _camera;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _camera = GetComponent<Camera>();
        _fieldOfView = _camera.fieldOfView;
    }

    private void LateUpdate()
    {
        Follow();
    }

    private void Follow()
    {
        if (_targets[0] == null || _targets[1] == null || GameManager.Instance.IsGameWined()) return;

        Vector3 targetsCenter = (_targets[0].position + _targets[1].position) / 2;

        Vector3 movePos = new Vector3(0, targetsCenter.y + _yOffset, transform.position.z);

        transform.position = Vector3.Lerp(transform.position, movePos, _moveSpeed);
    }

    public IEnumerator Zoom(float value, bool feverMode = false)
    {
        if(!feverMode)
        {
            _fieldOfView = _minFieldOfView + (_maxFieldOfView - _minFieldOfView) * value;
            _fieldOfView = Mathf.Clamp(_fieldOfView, _minFieldOfView, _maxFieldOfView);

            while (_camera.fieldOfView != _fieldOfView)
            {
                _camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, _fieldOfView, 0.0075f * Time.deltaTime);
                yield return _waitTime;
            }
        }
        else
        {
            _fieldOfView = 80;
            while (_camera.fieldOfView != _fieldOfView)
            {
                _camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, _fieldOfView, 0.01f * Time.deltaTime);
                yield return _waitTime;
            }
        }
    }
}