﻿using UnityEngine;

public class PlatformMovement : MonoBehaviour
{
    private bool _canMove;

    private float _moveSpeed = 2.5f;

    private void Start()
    {
        float r = Random.Range(0f, 1f);
        if(r < 0.5f)
        {
            _canMove = true;
        }
    }

    private void Update()
    {
        if(CanMove())
        {
            transform.Translate(Vector3.right * _moveSpeed * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!CanMove()) return;

        _moveSpeed *= -1;
    }

    private bool CanMove()
    {
        return _canMove;
    }
}
