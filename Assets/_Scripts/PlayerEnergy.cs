﻿using UnityEngine;

public class PlayerEnergy : MonoBehaviour
{
    public static PlayerEnergy Instance;

    [HideInInspector]
    public float _energyValue = 100, _currentEnergyValue = 100, _maxDragValue = 14f, _maxEnergyValue = 100, _collectedEnergyValue = 100;

    [SerializeField] private LiquidController _liduidController;

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        _energyValue = Mathf.Clamp(_energyValue, 0, _maxEnergyValue);
        _currentEnergyValue = Mathf.Clamp(_currentEnergyValue, 0, _maxEnergyValue);
        _collectedEnergyValue = Mathf.Clamp(_collectedEnergyValue, 0, _maxEnergyValue);

        _liduidController.ShiftNumber(_energyValue);
    }

    public void AddEnergy()
    {
        _energyValue++;
        _currentEnergyValue++;
    }

    public bool HasEnergy()
    {
        bool a = false;

        if(_energyValue > 2 || _collectedEnergyValue > 2)
        {
            a = true;
        }

        return a;
    }
}
