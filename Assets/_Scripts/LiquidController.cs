﻿using UnityEngine;

public class LiquidController : MonoBehaviour
{
    Vector3 lastPos;
    Vector3 velocity;
    Vector3 lastRot;
    Vector3 angularVelocity;
    public float MaxWobble = 0.1f;
    public float WobbleSpeed = 1f;
    public float Recovery = 5f;
    float wobbleAmountX;
    float wobbleAmountZ;
    float wobbleAmountToAddX;
    float wobbleAmountToAddZ;
    float pulse;
    float time = 0.5f;

    bool _rotate = true;

    [SerializeField] private Material _myMaterial;

    void Update()
    {
        if(_rotate)
        {
            time += Time.deltaTime;
            wobbleAmountToAddX = Mathf.Lerp(wobbleAmountToAddX, 0, Time.deltaTime * (Recovery));
            wobbleAmountToAddZ = Mathf.Lerp(wobbleAmountToAddZ, 0, Time.deltaTime * (Recovery));

            pulse = 2 * Mathf.PI * WobbleSpeed;
            wobbleAmountX = wobbleAmountToAddX * Mathf.Sin(pulse * time);
            wobbleAmountZ = wobbleAmountToAddZ * Mathf.Sin(pulse * time);


            _myMaterial.SetFloat("_WobbleX", wobbleAmountX);
            _myMaterial.SetFloat("_WobbleZ", wobbleAmountZ);

            velocity = (Time.deltaTime < Mathf.Epsilon) ? Vector3.zero : (lastPos - transform.position) / Time.deltaTime;
            angularVelocity = transform.rotation.eulerAngles - lastRot;

            wobbleAmountToAddX += Mathf.Clamp((velocity.x + (angularVelocity.z * 0.2f)) * MaxWobble, -MaxWobble, MaxWobble);
            wobbleAmountToAddZ += Mathf.Clamp((velocity.z + (angularVelocity.x * 0.2f)) * MaxWobble, -MaxWobble, MaxWobble);

            lastPos = transform.position;
            lastRot = transform.rotation.eulerAngles;
        }
    }

    public void ShiftNumber(float value)
    {
        float t = value / 100;
        float fillAmount = 0;

        if(t < 0.05f)
        { 
            _rotate = false;

            fillAmount = 1f;
        }
        else
        {
            _rotate = true;

            fillAmount = 0.9f - 0.9f * t;
        }

        _myMaterial.SetFloat("_FillAmount", fillAmount);
    }
}
