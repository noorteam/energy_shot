﻿using System.Collections;
using UnityEngine;

public class PerfectParticle : MonoBehaviour
{
    [SerializeField] private Material _material;
    [Space]

    [HideInInspector] public int _count;
    [Range(0f, 1f)]
    [SerializeField] private float _trailRatio;
    [SerializeField] private float _force;
    [SerializeField] private float _waitTime;
 
    [HideInInspector] public bool _dieParticle;
    [Space]

    [SerializeField] private GameObject _part;
    [Space]

    [HideInInspector] public Transform _moveTransform;

    private void Start()
    {
        MakeParticle();
    }

    private void Update()
    {
        if (transform.childCount == 1)
        {
            Destroy(gameObject);
        }
        if (_moveTransform == null && !_dieParticle)
        {
            Destroy(gameObject);
        }
    }

    private void MakeParticle()
    {

        for (int i = 0; i < _count; i++)
        {
            PlayerEnergy.Instance._collectedEnergyValue++;

            GameObject current = Instantiate(_part, transform.position, Quaternion.identity, transform);

            Rigidbody rigidbody = current.GetComponent<Rigidbody>();

            float x = Random.Range(-1f, 1f);
            float y = Random.Range(0f, 1f);
            float z = Random.Range(-1f, 1f);

            rigidbody.AddForce(new Vector3(x, y, z) * _force, ForceMode.Acceleration);

            float r = Random.Range(0f, 1f);
            if(r < _trailRatio)
            {
                current.transform.GetChild(0).gameObject.SetActive(true);
            }

            if(!_dieParticle)
            {
                StartCoroutine(MoveTo(current.transform));
            }
        }
    }

    private IEnumerator MoveTo(Transform current)
    {
        yield return new WaitForSeconds(_waitTime);

        current.GetComponent<Rigidbody>().isKinematic = true;

        while (_moveTransform != null && Vector3.Distance(current.position, _moveTransform.position) > 0.1f)
        { 
            current.position = Vector3.MoveTowards(current.position, _moveTransform.position, 25 * Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
        }

        PlayerEnergy.Instance.AddEnergy();

        Destroy(current.gameObject);
    }
}
