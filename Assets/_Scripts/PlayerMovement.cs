﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement Instance;

    private float _shootImpulse = 5000f;
    private float _dragSpeed = 35;
    private float _dragRadius = 2;
    private float _startDragRadius;

    private bool _touched;

    private Vector3 _inputStartPosition;
    private Vector3 _inputCurrentPosition;
    private Vector3 _inputDelta;

    public Rigidbody _rigidbody;

    public GameObject _directionObject;

    private PlayerLegController _playerLegController;
    private PlayerEnergy _playerEnergy;
    private PlayerFever _playerFever;
    private GameManager _gameManager;
    private CameraMovement _cameraMovement;

    [SerializeField] private Material _arrowMaterial;

    [SerializeField] private SphereCollider _collider;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _playerLegController = PlayerLegController.Instance;
        _playerEnergy = PlayerEnergy.Instance;
        _playerFever = PlayerFever.Instance;
        _gameManager = GameManager.Instance;
        _cameraMovement = CameraMovement.Instance;

        _startDragRadius = _dragRadius;
    }

    private void FixedUpdate()
    {
        if (_gameManager.IsGameStarted() && IsTouched() && Vector3.Distance(_inputStartPosition, MousePosition()) > 50f)
        {
            Drag();
        }
    }

    private void Update()
    {
        if (_gameManager.IsGameStarted() && !_playerFever.IsFeverShooted() && _playerEnergy.HasEnergy())
        {
            MouseAction();
        }

        if(!_playerFever.IsFeverMode())
        {
            _collider.enabled = true;
        }
    }


    private void AddImpulse()
    {
        _rigidbody.useGravity = true;
        _directionObject.SetActive(false);

        if(!_playerFever.IsFeverMode())
        {
            StartCoroutine(_cameraMovement.Zoom(0));

            _playerEnergy._collectedEnergyValue -= _playerEnergy._currentEnergyValue - _playerEnergy._energyValue;
            _playerEnergy._currentEnergyValue = _playerEnergy._energyValue;

            float magnitude = Vector3.Distance(transform.position, _playerLegController._shootCenter);
            _rigidbody.AddForce(transform.up * magnitude * _shootImpulse, ForceMode.Acceleration);
        }
        else
        {
            StartCoroutine(_cameraMovement.Zoom(0, true));
            _collider.enabled = false;
            _playerFever.ShootedFever();

            _rigidbody.AddForce(transform.up * _playerFever._feverModeImpulse, ForceMode.Acceleration);
        }
    }

    private void Drag()
    {
        _directionObject.SetActive(true);
        _rigidbody.useGravity = false;

        if(!_playerFever.IsFeverMode())
        {
            RotateBody();
        }
        _playerLegController.OffSprings();
        SetDragRadius();

        Vector2 newPos = _inputDelta;
        newPos = Vector2.ClampMagnitude(newPos, 15);
        _rigidbody.AddForce(newPos * _dragSpeed * Time.fixedDeltaTime, ForceMode.VelocityChange);
        _rigidbody.velocity = Vector3.Lerp(_rigidbody.velocity, Vector3.zero, 0.7f);
        _rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, 15);
    }

    private void SetDragRadius()
    {
        float distance = Vector3.Distance(transform.position, _playerLegController._shootCenter);

        if (distance > _dragRadius)
        {
            Vector3 fromOriginToObject = transform.position - _playerLegController._shootCenter;
            fromOriginToObject *= _dragRadius / distance;
            transform.position = _playerLegController._shootCenter + fromOriginToObject;
        }

        SetDragEnergy(distance);
    }

    private void SetDragEnergy(float distance)
    {
        float part = distance / _startDragRadius;

        if(!_playerFever.IsFeverMode())
        {
            _playerEnergy._energyValue = _playerEnergy._currentEnergyValue - _playerEnergy._maxDragValue * part;

            if (_playerEnergy._currentEnergyValue >= _playerEnergy._maxDragValue)
            {
                _dragRadius = _startDragRadius;
            }
            else
            {
                _dragRadius = _startDragRadius * (_playerEnergy._currentEnergyValue / _playerEnergy._maxDragValue);
            }

        }

        ArrowFillAmount(part);
        StartCoroutine(_cameraMovement.Zoom(part));
    }

    private void RotateBody()
    {
        transform.LookAt(_playerLegController._shootCenter);
        transform.rotation = Quaternion.FromToRotation(Vector3.up, transform.forward);
        transform.localRotation = Quaternion.Euler(0, 0, transform.localEulerAngles.z);
    }

    public void ReduceVelocity()
    {
        _rigidbody.velocity = _rigidbody.velocity * -0.125f;
    }

    private void ArrowFillAmount(float value)
    {
        float fillAmount = 1.15f - 1.35f * value;
        _arrowMaterial.SetFloat("_FillAmount", fillAmount);
    }


    private void MouseAction()
    {   
        if (Input.GetMouseButtonDown(0))
        {
            _touched = true;
            _inputCurrentPosition = MousePosition();
            _inputStartPosition = MousePosition();

            if (!_playerLegController.LegsShooted())
            {
                _playerLegController.ShootLegs();
                transform.rotation = Quaternion.identity;
            }
        }
        if (Input.GetMouseButton(0))
        {
            _inputDelta = MousePosition() - _inputCurrentPosition;
            _inputCurrentPosition = MousePosition();
        }
        if (Input.GetMouseButtonUp(0) && _touched)
        {
            _touched = false;
            _inputDelta = _inputStartPosition - MousePosition();

            _playerLegController.GatherLegs();
            AddImpulse();
        }
    }

    private Vector3 MousePosition()
    {
        return Input.mousePosition;
    }

    public bool IsTouched()
    {
        return _touched;
    }
}
