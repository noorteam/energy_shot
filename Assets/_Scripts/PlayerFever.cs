﻿using System.Collections;
using UnityEngine;

public class PlayerFever : MonoBehaviour
{
    public static PlayerFever Instance;

    public float _feverModeImpulse = 15000f;
    private float _currentFillAmount = 360f;
    private float _fillAmount;
    private float _screenColorValue = 0.7f;
    WaitForSeconds _waitTime = new WaitForSeconds(0.001f);

    private int _perfectShootedCount;

    private bool _feverMode;
    private bool _shootedFever;

    [SerializeField] private Material _feverProgressMaterial;
    [SerializeField] private Material _feverScreenMaterial;
    [Space]

    [SerializeField] private ParticleSystem[] _feverFX;
    [Space]

    [SerializeField] private Light _feverLight;
    [Space]

    [SerializeField] private GameObject[] _feverWalls;

    private PlayerMovement _playerMovement;
    private CameraMovement _cameraMovement;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _playerMovement = PlayerMovement.Instance;
        _cameraMovement = CameraMovement.Instance;

        _feverProgressMaterial.SetFloat("_Arc2", _currentFillAmount);
        _feverScreenMaterial.SetFloat("_MinRadius", _screenColorValue);
    }

    private void Update()
    {
        if (_perfectShootedCount == 3)
        {
            _feverMode = true;

            SpawnWalls();
            StartCoroutine(FeverScreenColor());

            _perfectShootedCount = 0;
        }
        if(!IsFeverMode())
        {
            if(Mathf.Abs(_screenColorValue - 0.7f) > 0.01f)
            {
                _screenColorValue = Mathf.MoveTowards(_screenColorValue, 0.7f, 0.3f * Time.deltaTime);
                _feverScreenMaterial.SetFloat("_MinRadius", _screenColorValue);
            }
        }

        if (IsFeverMode() && IsFeverShooted())
        {
            transform.rotation = Quaternion.identity;
            if (_playerMovement._rigidbody.velocity.y < -2)
            {
                _shootedFever = false;
                _feverMode = false;
                StartCoroutine(_cameraMovement.Zoom(0));

                _feverFX[2].Stop(true);
                _feverLight.enabled = false;
            }
        }

        FeverProgressBar();
    }


    public void ChangePerfectShootedCount(bool add)
    {
        if (add)
        {
            VibrationController.Instance.MediumImpact();
            _perfectShootedCount++;

            _feverFX[_perfectShootedCount - 1].Play(true);
            if (_perfectShootedCount - 2 >= 0)
            {
                _feverFX[_perfectShootedCount - 2].Stop(true);
            }
        }
        else
        {
            if (_perfectShootedCount > 0)
            {
                _perfectShootedCount--;

                _feverFX[_perfectShootedCount].Stop(true);
                if (_perfectShootedCount > 0)
                {
                    _feverFX[_perfectShootedCount - 1].Play(true);
                }
            }
        }
    }


    private void FeverProgressBar()
    {
        _fillAmount = 360 - (float)_perfectShootedCount / 3 * 360;

        if (Mathf.Abs(_currentFillAmount - _fillAmount) > 0.1f)
        {
            _currentFillAmount = Mathf.MoveTowards(_currentFillAmount, _fillAmount, 500f * Time.deltaTime);
            _feverProgressMaterial.SetFloat("_Arc2", _currentFillAmount);
        }
    }

    private IEnumerator FeverScreenColor()
    {
        float r = Random.Range(0.2f, 0.5f);

        while(Mathf.Abs(r - _screenColorValue) > 0.01f)
        {
            if(!IsFeverMode())
            {
                break;
            }

            _screenColorValue = Mathf.MoveTowards(_screenColorValue, r, 0.5f * Time.deltaTime);
            _feverScreenMaterial.SetFloat("_MinRadius", _screenColorValue);

            yield return _waitTime;
        }

        if(IsFeverMode())
        {
            StartCoroutine(FeverScreenColor());
        }
    }


    private void SpawnWalls()
    {
        Vector3 spawnPos = new Vector3(0, transform.position.y + 15f, 0);

        for (int i = 0; i < 10; i++)
        {
            int r = Random.Range(0, 2);
            Instantiate(_feverWalls[r], spawnPos, Quaternion.identity);
            spawnPos += new Vector3(0, 0.95f);
        }
    }


    public bool IsFeverMode()
    {
        return _feverMode;
    }

    public void ShootedFever()
    {
        _feverLight.enabled = true;
        _shootedFever = true;
    }

    public bool IsFeverShooted()
    {
        return _shootedFever;
    }
}
