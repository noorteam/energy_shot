﻿using UnityEngine;
using MoreMountains.NiceVibrations;

public class VibrationController : MonoBehaviour
{
    public static VibrationController Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void HeavyImpact()
    {
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
        if (MMVibrationManager.HapticsSupported())
        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
#endif
    }

    public void MediumImpact()
    {
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
        if (MMVibrationManager.HapticsSupported())
        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
#endif
    }

    public void FalureImpact()
    {
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
        if (MMVibrationManager.HapticsSupported())
        MMVibrationManager.Haptic(HapticTypes.Failure);
#endif
    }
}
