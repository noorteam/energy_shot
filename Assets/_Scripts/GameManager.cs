﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    private int _currentLevel;
    private int _lastLevel;

    private bool _gameStarted;
    private bool _gameWined;

    private UIController _uiController;
    private PlayerEnergy _playerEnergy;
    private PlayerMovement _playerMovement;

    //[SerializeField] private GameObject _wall;
    [SerializeField] private GameObject _perfectParicle;
    [SerializeField] private GameObject _breakedSphere;

    private Transform _finishTransform;


    private void Awake()
    {
        Instance = this;

        _lastLevel = PlayerPrefs.GetInt("Last_Level");
        _currentLevel = PlayerPrefs.GetInt("Level");
        if (_currentLevel == 0)
        {
            _currentLevel = 1;
            PlayerPrefs.SetInt("Level", _currentLevel);
        }
        LOAD_LEVEL();
    }

    private void Start()
    {
        _uiController = UIController.Instance;
        _playerEnergy = PlayerEnergy.Instance;
        _playerMovement = PlayerMovement.Instance;

        _finishTransform = GameObject.Find("finish").transform;

        //SpawnWall();
    }

    private void Update()
    {
        if(IsGameStarted() && !_playerEnergy.HasEnergy() && _playerMovement._rigidbody.velocity.y < -10)
        {
            GameOver();
        }
    }


    public void SpawnDieParticle()
    {
        GameOver();

        GameObject current = Instantiate(_breakedSphere, _playerMovement.transform.position, Quaternion.identity);

        List<Rigidbody> _currentParts = new List<Rigidbody>();
        _currentParts.AddRange(current.GetComponentsInChildren<Rigidbody>());

        foreach (Rigidbody item in _currentParts)
        {
            item.AddExplosionForce(1250, _playerMovement.transform.position, 10);
        }

        SpawnPerfectParticle((int)PlayerEnergy.Instance._energyValue, true);

        Destroy(current, 2f);
    }

    public void SpawnPerfectParticle(int count, bool die)
    {
        GameObject current = Instantiate(_perfectParicle, _playerMovement.transform.position, Quaternion.identity);

        PerfectParticle fx = current.GetComponent<PerfectParticle>();

        fx._moveTransform = _playerMovement.transform;
        fx._count = count;
        fx._dieParticle = die;
    }

    public void SpawnGlassParticle(Collider other)
    {
        if (other.transform.parent.childCount < 3) return;

        Transform breakedMesh = other.transform.parent.GetChild(2).transform;

        breakedMesh.SetParent(null);
        breakedMesh.gameObject.SetActive(true);

        List<Rigidbody> _currentParts = new List<Rigidbody>();
        _currentParts.AddRange(breakedMesh.GetComponentsInChildren<Rigidbody>());

        foreach (Rigidbody item in _currentParts)
        {
            item.AddExplosionForce(1250, other.ClosestPoint(_playerMovement.transform.position), 5);
        }
        Destroy(breakedMesh.gameObject, 2f);

        Destroy(other.transform.parent.gameObject);
    }

    public void SpawnObstaclePartcile(Collider other)
    {
        if (other.transform.parent.childCount < 4) return;

        Transform breakedMesh = other.transform.parent.GetChild(3).transform;

        breakedMesh.SetParent(null);
        breakedMesh.gameObject.SetActive(true);

        List<Rigidbody> _currentParts = new List<Rigidbody>();
        _currentParts.AddRange(breakedMesh.GetComponentsInChildren<Rigidbody>());

        foreach (Rigidbody item in _currentParts)
        {
            item.AddExplosionForce(2000, other.ClosestPoint(_playerMovement.transform.position), 10);
        }
        Destroy(breakedMesh.gameObject, 2f);

        Destroy(other.transform.parent.gameObject);
    }

    public void SpawnFeverWallPartcile(Collider other)
    {
        if (other.transform.parent.childCount < 2) return;

        Transform breakedMesh = other.transform.parent.GetChild(1).transform;

        breakedMesh.SetParent(null);
        breakedMesh.gameObject.SetActive(true);

        List<Rigidbody> _currentParts = new List<Rigidbody>();
        _currentParts.AddRange(breakedMesh.GetComponentsInChildren<Rigidbody>());

        foreach (Rigidbody item in _currentParts)
        {
            item.AddExplosionForce(2000, other.ClosestPoint(_playerMovement.transform.position), 10);
        }
        Destroy(breakedMesh.gameObject, 2f);

        Destroy(other.transform.parent.gameObject);
    }


    public void GameStart()
    {
        _gameStarted = true;
    }

    public void GameOver()
    {
        if (!_gameStarted) return;
        _gameStarted = false;

        StartCoroutine(_uiController.GameOver());
    }

    public void GameWin()
    {
        if (!_gameStarted) return;
        _gameStarted = false;
        _gameWined = true;

        _currentLevel++;
        PlayerPrefs.SetInt("Level", _currentLevel);
        PlayerPrefs.SetInt("Last_Level", 0);

        StartCoroutine(_uiController.GameWin());

        _finishTransform.GetChild(0).GetComponent<ParticleSystem>().Play(true);
    }


    public bool IsGameStarted()
    {
        return _gameStarted;
    }
    public bool IsGameWined()
    {
        return _gameWined;
    }

    public int GetCurrentLevel()
    {
        return _currentLevel;
    }

    public float PlayerProgress()
    {
        return _playerMovement.transform.position.y / _finishTransform.position.y;
    }

    private void LOAD_LEVEL()
    {
        if (_currentLevel <= SceneManager.sceneCountInBuildSettings - 1)
        {
            SceneManager.LoadScene(_currentLevel, LoadSceneMode.Additive);
        }
        else
        {
            int r = 0;
            if (_lastLevel != 0)
            {
                r = _lastLevel;
            }
            else
            {
                r = Random.Range(1, SceneManager.sceneCountInBuildSettings);
                _lastLevel = r;
                PlayerPrefs.SetInt("Last_Level", _lastLevel);
            }
            SceneManager.LoadScene(r, LoadSceneMode.Additive);
        }
    }
    public void LOAD_GENERAL()
    {
        SceneManager.LoadScene(0);
    }
 
    /*private void SpawnWall()
    {
        GameObject parent = new GameObject("wall");

        GameObject last = null;

        for (int i = 0; i < 70; i++)
        {
            Vector3 spawnPos = new Vector3(0, 4.5f, 0.25f);

            if(i > 0)
            {
                spawnPos = new Vector3(0, last.transform.position.y + 4.334f, 0.25f);
            }

            GameObject current = Instantiate(_wall, spawnPos, _wall.transform.rotation, parent.transform);

            int r = Random.Range(0, 2);

            if(r == 0)
            {
                current.transform.localScale = new Vector3(0.5f, -0.5f, 0.355f);
                current.transform.position = new Vector3(0.1854f, current.transform.position.y, current.transform.position.z);
            }
            last = current;
        }
    }*/
}
